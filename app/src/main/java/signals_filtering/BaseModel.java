package signals_filtering;

import java.io.*;
import javax.imageio.ImageIO;

import signals_filtering.entity.Model;

import java.awt.image.BufferedImage;

public class BaseModel implements Model {
    BufferedImage firstOrderImage;
    BufferedImage secondOrderImage;
    int width;
    int height;

    public void openFile(String filePathOpen) {
        try {
            File inputFile = new File(filePathOpen);
            firstOrderImage = ImageIO.read(inputFile);
            width = firstOrderImage.getWidth();
            height = firstOrderImage.getHeight();
        } catch(Exception e) {
            e.getMessage();
        }
    }

    public void saveFile(String filePathSave) {
        try {
            File outputFile = new File(filePathSave);
            ImageIO.write(secondOrderImage, "jpg", outputFile);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public BufferedImage getFirstOrder() {
        return firstOrderImage;
    }

    public BufferedImage getSecondOrder() {

        return secondOrderImage;
    }
}

