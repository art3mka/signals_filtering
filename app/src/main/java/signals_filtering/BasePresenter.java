package signals_filtering;

import java.awt.image.BufferedImage;

import signals_filtering.entity.Presenter;


public class BasePresenter implements Presenter {
    BaseModel model;
    
    public BasePresenter(BaseModel model) {
        this.model = model;
    }

    public void handleOpen(String filePathOpen) {
        model.openFile(filePathOpen);
    }

    public void handleSave(String filePathSave) {
        model.saveFile(filePathSave);
    }

    public BufferedImage handleFirst() {
        BufferedImage modifiedImage = model.getFirstOrder();
        return modifiedImage;
    }

    public BufferedImage handleSecond() {
        BufferedImage modifiedImage = model.getSecondOrder();
        return modifiedImage;
    }
}

