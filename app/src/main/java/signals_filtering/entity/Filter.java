package signals_filtering.entity;

public abstract class Filter {
    Signal signal;

    public Filter(Signal signal) {
        this.signal = signal;
    }

    public void applyFilter() {}
}

