package signals_filtering.entity;

public interface Presenter {
    void handleOpen(String filePath);

    void handleSave(String filePath);
}
