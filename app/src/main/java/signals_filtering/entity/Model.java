package signals_filtering.entity;

public interface Model {
    void openFile(String filePath);

    void saveFile(String filePath);
}