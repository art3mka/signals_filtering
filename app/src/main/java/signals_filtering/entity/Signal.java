package signals_filtering.entity;

import java.awt.image.BufferedImage;

public abstract class Signal {

    BufferedImage firstOrderSignal;
    BufferedImage SecondOrderSignal;
    
    public Signal(BufferedImage firstOrderSignal) {
        this.firstOrderSignal = firstOrderSignal;
    }

    public BufferedImage getFirstOrderSignal() {
        return firstOrderSignal;
    }
    public void setFirstOrderSignal(BufferedImage firstOrderSignal) {
        this.firstOrderSignal = firstOrderSignal;
    }
    public BufferedImage getSecondOrderSignal() {
        return SecondOrderSignal;
    }
    public void setSecondOrderSignal(BufferedImage SecondOrderSignal) {
        this.SecondOrderSignal = SecondOrderSignal;
    }
}

