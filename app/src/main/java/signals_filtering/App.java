package signals_filtering;

public class App {
    public static void run() {
        BaseModel model = new BaseModel();
        BasePresenter presenter = new BasePresenter(model);
        BaseView view = new BaseView(presenter);
        view.init();
    }
}
