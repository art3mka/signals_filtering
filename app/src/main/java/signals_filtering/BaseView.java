package signals_filtering;

import signals_filtering.entity.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

public class BaseView extends JFrame implements View{
    BasePresenter presenter;

    public BaseView(BasePresenter presenter) {
        this.presenter = presenter;
    }

    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");
        JMenuItem open = new JMenuItem("Open");
        JMenuItem save = new JMenuItem("Save");

        fileMenu.add(open);
        fileMenu.add(save);

        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                JFileChooser fileChooser = new JFileChooser();
                int response = fileChooser.showOpenDialog(null);

                if (response == JFileChooser.APPROVE_OPTION) {
                    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                    presenter.handleOpen(filePath);
                    
                    JTextField field = new JTextField();
                    field.setHorizontalAlignment(JTextField.CENTER);
                    field.setText("Data file loaded!");
                    Font f = new Font("Dialog", Font.PLAIN, 30);
                    field.setFont(f);

                    getContentPane().removeAll();
                    getContentPane().add(BorderLayout.CENTER, field);
                    setVisible(true);
                }               
            }
        });

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {

                JFileChooser fileChooser = new JFileChooser();
                int response = fileChooser.showSaveDialog(null);

                if (response == JFileChooser.APPROVE_OPTION) {
                    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                    presenter.handleSave(filePath);
                }
            }
        });

        return fileMenu;
    }

    private JMenu createFiltersMenu() {
        JMenu viewMenu = new JMenu("Digital filters");

        JMenuItem firstOrder = new JMenuItem("First Order filter");
        JMenuItem secondOrder = new JMenuItem("Second Order filter");

        viewMenu.add(firstOrder);
        viewMenu.add(secondOrder);

        firstOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String firstPath = "app/src/main/java/signals_filtering/images/firstOrder.png";
                getContentPane().removeAll();
                getContentPane().add(new JLabel(new ImageIcon(firstPath)));
                setVisible(true);
            }
        });

        secondOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String secondPath = "app/src/main/java/signals_filtering/images/secondOrder.jpg";
                getContentPane().removeAll();
                getContentPane().add(new JLabel(new ImageIcon(secondPath)));
                setVisible(true);
            }
        });

        return viewMenu;
    }

    public void init()
    {
        JFrame.setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Image Processing");

        JMenuBar menuBar = new JMenuBar();

        menuBar.add(createFileMenu());
        menuBar.add(createFiltersMenu());

        setJMenuBar(menuBar);

        setSize(1920, 1080);
        setVisible(true);
    }
}
