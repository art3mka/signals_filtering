#### To build app: 

```
.\gradlew clean build
```

#### To run jar:

```
java -jar .\app\build\libs\app.jar
```
